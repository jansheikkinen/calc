# calc

a simple calculator that demonstrates the general process for making a
programming language

the general process is as follows:
1. transform source code to a list of lexical tokens
2. parse the list of tokens to create an intermediate representation called
an abstract syntax tree
3. analyse and optimise the tree (not shown here) to ensure correctness and
proper efficiency
4. either run the tree directly, or compile it to a different representation
that *can* be run; both are demonstrated below

BNF GRAMMAR:
```ebnf
EXPRESSION := TERMINAL? EOF
TERMINAL   := FACTOR   (("+" | "-") FACTOR)*
FACTOR     := UNARY    (("*" | "/") UNARY)*
UNARY      := PRIMARY | ("+" | "-") UNARY
PRIMARY    := NUMBER  | "(" EXPRESSION ")"
```

this article explains the syntax above:
<https://en.wikipedia.org/wiki/Backus-Naur_form>

otherwise all you need to know is it means the following are valid:
```
1
-1
1 + 2
1 * 2
(1 + 2) * 3
((1 + 2) * 3) - -4
```


## tokenisation
this step takes in a source string (what the user typed directly) and
attempts to turn it into a list of lexical tokens[1].
once one has a list of tokens, they can then be passed to the parser to both
ensure the input is grammatically correct, and to get a semantic
representation of the program (see parser function for more on that)

references:
1. <https://en.wikipedia.org/wiki/Lexical_analysis>


## parsing
this step takes in a list of lexical tokens created by the tokenisation step
and uses recursive descent[1] parsing to return an abstract syntax tree[2].
once you have the ast, you can then do one of the following:
- perform analysis or optimisations, which ensure the tree is semantically
correct and modifies the tree to remove redundant operations
- interpret it using a tree-walking[3] interpreter, which is conceptually
similar to this parser, but also quite slow
- compile to another representation, such as machine code or bytecode[4]

references:
1. <https://en.wikipedia.org/wiki/Recursive_descent_parser>
2. <https://en.wikipedia.org/wiki/Abstract_syntax_tree>
3. <https://en.wikipedia.org/wiki/Tree_traversal>
4. <https://en.wikipedia.org/wiki/Bytecode>


## bytecode interpreter
NOTE: quite frankly this step is *extremely* complicated and i don't think
my explanations are very good, so feel free to skip this step and go to the
next section, or read crafting interpreters for a much better explanation

this section is actually two steps:
1. transform the ast into bytecode[1]
2. evaluate that bytecode

bytecode is generally similar to machine code, but uses a made-up instruction
set designed specifically for your programming language's needs.
to run the bytecode, we feed it to a program called a virtual machine[2],
which basically emulates a cpu that understands the instruction set.
virtual machines are also how you emulate i.e. android games on windows

the two main forms of bytecode are:
- stack-based[3], which are simpler to implement, but oftentimes slower
- register-based[4], which are more complex but often faster; they more
closely resemble real cpu instruction sets

python and java use the stack-based approach, as well as this program.
lua uses a register-based approach

references:
1. <https://en.wikipedia.org/wiki/Bytecode>
2. <https://en.wikipedia.org/wiki/Virtual_machine>
3. <https://en.wikipedia.org/wiki/Stack_machine>
4. <https://en.wikipedia.org/wiki/Register_machine>


## tree-walk interpreter
you can actually walk through the tree[1], much like the parser, but in
reverse, in order to evaluate expressions bottom-up.
this method is generally the slowest possible way of executing code, but its
also conceptually the simplest, and works really well for a simple program
like this one

references:
1. <https://en.wikipedia.org/wiki/Tree_traversal>
